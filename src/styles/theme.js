import { createTheme } from "@mui/material";

const theme = createTheme({
  palette: {
    primary: {
      main: "#43ffaf",
    },
    background: {
      default: "#262A33",
      main: "#1f232c",
    },
    text: {
      primary: "#e5f7ef",
      secondary: "#526777",
      delete: "#FF0080",
    },
  },
});

export default theme;
