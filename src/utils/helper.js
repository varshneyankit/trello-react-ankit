import axios from "axios";

export const handleGetRequest = async (API_URL) => {
  const response = await axios.get(API_URL);
  return response;
};

export const handlePostRequest = async (API_URL) => {
  const response = await axios.post(API_URL);
  return response;
};

export const handlePutRequest = async (API_URL) => {
  const response = await axios.put(API_URL);
  return response;
};

export const handleDeleteRequest = async (API_URL) => {
  const response = await axios.delete(API_URL);
  return response;
};

export const getCheckedItemsPercent = (checkItems) => {
  let checkedCount = 0;
  let totalCheckItems = checkItems.length;

  checkItems.forEach((checkItem) => {
    if (checkItem.state == "complete") {
      checkedCount++;
    }
  });

  return (checkedCount * 100) / totalCheckItems;
};
