import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import toast from "react-hot-toast";
import { Box, Container, Grid, IconButton, Typography } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import BasicModal from "../common/BasicModal";
import Loader from "../common/Loader";
import Toast from "../common/Toast";
import CardsSection from "../CardsSection";
import { KEY, TOKEN } from "../../constants";
import {
  handleGetRequest,
  handlePostRequest,
  handlePutRequest,
} from "../../utils/helper";
import theme from "../../styles/theme";

function ListsPage() {
  const [lists, setLists] = useState([]);
  const [listName, setListName] = useState("");
  const [loading, setLoading] = useState(true);

  const { boardId } = useParams();

  useEffect(() => {
    function getLists() {
      const API_GET_LISTS = `https://api.trello.com/1/boards/${boardId}/lists?key=${KEY}&token=${TOKEN}`;

      handleGetRequest(API_GET_LISTS)
        .then((response) => {
          setLists(response.data);
        })
        .catch((error) => {
          toast.error("Unable to get lists!");
        })
        .finally(() => {
          setLoading(false);
        });
    }

    getLists();
  }, []);

  function createList() {
    const newListName = listName.trim();
    const API_CREATE_LIST = `https://api.trello.com/1/lists?name=${newListName}&idBoard=${boardId}&key=${KEY}&token=${TOKEN}`;

    setLoading(true);

    handlePostRequest(API_CREATE_LIST)
      .then((response) => {
        setLists([response.data, ...lists]);
      })
      .catch((error) => {
        toast.error("Unable to create list!");
      })
      .finally(() => {
        setListName("");
        setLoading(false);
      });
  }

  function deleteList(listId) {
    const API_DELETE_LIST = `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${KEY}&token=${TOKEN}`;

    setLoading(true);

    handlePutRequest(API_DELETE_LIST)
      .then((response) => {
        setLists(lists.filter((list) => list.id !== listId));
      })
      .catch((error) => {
        toast.error("Unable to delete list!");
      })
      .finally(() => {
        setLoading(false);
      });
  }

  return (
    <Container sx={{ mt: 8 }}>
      <Loader status={loading} />
      <Toast />
      <>
        <Grid container sx={{}}>
          <Grid item flexGrow={1} alignContent={"center"}>
            <Typography
              variant="h4"
              sx={{ color: theme.palette.text.secondary }}
            >
              Lists
            </Typography>
          </Grid>
          <Grid item>
            <BasicModal
              containerText="Create List"
              containerStyle={{
                backgroundColor: theme.palette.primary.main,
                borderRadius: 40,
                color: theme.palette.background.main,
                padding: "14px 24px",
                cursor: "pointer",
                "&:hover": {
                  backgroundColor: theme.palette.text.primary,
                },
              }}
              header={"Create List"}
              inputPlaceholder={"Enter List Name"}
              inputValue={listName}
              handleInputChange={setListName}
              handleSubmit={createList}
            />
          </Grid>
        </Grid>
        <Grid
          container
          direction="row"
          justifyContent="flex-start"
          alignItems="flex-start"
          flexWrap={"nowrap"}
          overflow={"auto"}
          mt={2}
          p={2}
        >
          {lists.map((list, index) => {
            let listId = list.id;
            let listName = list.name;

            return (
              <Grid
                item
                key={listId}
                sx={{
                  backgroundColor: theme.palette.background.main,
                  borderRadius: "6px",
                  flexShrink: "0",
                  marginRight: "8px",
                  padding: "10px 16px",
                  width: "280px",
                }}
              >
                <Box
                  display="flex"
                  alignItems={"center"}
                  sx={{ marginBottom: "16px" }}
                >
                  <Typography variant="h6" flexGrow={1}>
                    {listName}
                  </Typography>
                  <IconButton
                    disabled={loading}
                    onClick={() => deleteList(listId)}
                  >
                    <CloseIcon
                      fontSize="small"
                      sx={{
                        color: theme.palette.text.secondary,
                        "&:hover": {
                          color: theme.palette.text.delete,
                        },
                      }}
                    />
                  </IconButton>
                </Box>
                <CardsSection listId={listId} />
              </Grid>
            );
          })}
        </Grid>
      </>
    </Container>
  );
}

export default ListsPage;
