import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import toast from "react-hot-toast";
import { Box, Container, Grid, Typography } from "@mui/material";
import BasicModal from "../common/BasicModal";
import Loader from "../common/Loader";
import Toast from "../common/Toast";
import { KEY, TOKEN } from "../../constants";
import { handleGetRequest, handlePostRequest } from "../../utils/helper";
import theme from "../../styles/theme";

function BoardsPage() {
  const [boards, setBoards] = useState([]);
  const [boardName, setBoardName] = useState("");
  const [loading, setLoading] = useState(true);

  const navigate = useNavigate();

  useEffect(() => {
    function getBoards() {
      const API_GET_BOARDS = `https://api.trello.com/1/members/me/boards?key=${KEY}&token=${TOKEN}`;

      handleGetRequest(API_GET_BOARDS)
        .then((response) => {
          setBoards(response.data);
        })
        .catch((error) => {
          toast.error("Unable to get boards!");
        })
        .finally(() => {
          setLoading(false);
        });
    }

    getBoards();
  }, []);

  function createBoard() {
    const newBoardName = boardName.trim();
    const API_CREATE_BOARD = `https://api.trello.com/1/boards/?name=${newBoardName}&key=${KEY}&token=${TOKEN}`;

    setLoading(true);

    handlePostRequest(API_CREATE_BOARD)
      .then((response) => {
        const boardId = response.data.id;
        navigate(`/boards/${boardId}`);
      })
      .catch((error) => {
        if (boards.length === 10) {
          toast.error("Boards creation limit exceeded!");
        } else {
          toast.error("Unable to create board!");
        }
      })
      .finally(() => {
        setBoardName("");
        setLoading(false);
      });
  }

  return (
    <>
      <Container sx={{ maxWidth: 1200, mt: 8 }}>
        <Loader status={loading} />
        <Toast />
        <Grid container>
          <Grid item flexGrow={1} alignContent={"center"}>
            <Typography
              variant="h4"
              sx={{ color: theme.palette.text.secondary }}
            >
              Boards
            </Typography>
          </Grid>
          <Grid item>
            <BasicModal
              containerText="Create Board"
              containerStyle={{
                backgroundColor: theme.palette.primary.main,
                borderRadius: 40,
                color: theme.palette.background.main,
                padding: "14px 24px",
                cursor: "pointer",
                "&:hover": {
                  backgroundColor: theme.palette.text.primary,
                },
              }}
              header={"Create Board"}
              inputPlaceholder={"Enter Board Name"}
              inputValue={boardName}
              handleInputChange={setBoardName}
              handleSubmit={createBoard}
            />
          </Grid>
        </Grid>
        <Grid container spacing={2} mt={2}>
          {boards.map((board, index) => {
            let boardId = board.id;
            let boardName = board.name;

            return (
              <Grid item key={boardId} xs={12} sm={6} md={3}>
                <Link to={`/boards/${boardId}`}>
                  <Box
                    sx={{
                      backgroundColor: theme.palette.background.main,
                      borderRadius: "8px",
                      color: theme.palette.text.primary,
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      minHeight: "120px",
                      width: "100%",
                      "&:hover": {
                        color: theme.palette.primary.main,
                      },
                    }}
                  >
                    <Typography variant="h6">{boardName}</Typography>
                  </Box>
                </Link>
              </Grid>
            );
          })}
        </Grid>
      </Container>
    </>
  );
}

export default BoardsPage;
