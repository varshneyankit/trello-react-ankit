import { Link, useLocation } from "react-router-dom";
import { AppBar, Grid, Toolbar, Typography } from "@mui/material";
import trelloIcon from "../../public/trelloIcon.svg";
import theme from "../styles/theme";

function Navbar() {
  const location = useLocation();

  return (
    <AppBar
      position="static"
      elevation={0}
      sx={{
        backgroundColor: theme.palette.background.default,
        color: theme.palette.text.primary,
      }}
    >
      <Toolbar
        sx={{
          display: "flex",
          width: "100%",
          maxWidth: 1200,
          mx: "auto",
          paddingTop: "20px",
        }}
      >
        <Grid
          sx={{
            display: "flex",
            alignItems: "center",
            flexGrow: 1,
          }}
        >
          <img src={trelloIcon} alt="Trello SVG" height={"25"} width={"25"} />
          <Typography variant="h5" marginLeft={1}>
            trello
          </Typography>
        </Grid>
        <Grid
          sx={{
            display: "flex",
            alignItems: "center",
          }}
        >
          {location.pathname != "/" && (
            <Link
              to={"/"}
              style={{
                color: theme.palette.text.primary,
              }}
            >
              <Typography variant="h6">Boards</Typography>
            </Link>
          )}
        </Grid>
      </Toolbar>
    </AppBar>
  );
}

export default Navbar;
