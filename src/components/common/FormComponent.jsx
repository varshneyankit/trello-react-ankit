import { IconButton } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import theme from "../../styles/theme";

function FormComponent({
  loading,
  inputPlaceholder,
  inputValue,
  handleInputChange,
  handleSubmit,
}) {
  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        handleSubmit();
      }}
      style={{
        backgroundColor: theme.palette.background.default,
        borderRadius: "4px",
        display: "flex",
        justifyContent: "space-between",
        padding: "4px 12px",
        width: "100%",
      }}
    >
      <input
        placeholder={inputPlaceholder}
        value={inputValue}
        disabled={loading}
        onChange={(e) => handleInputChange(e.target.value)}
        style={{
          backgroundColor: "transparent",
          border: "none",
          color: theme.palette.text.primary,
          fontFamily: "Roboto",
          fontSize: 16,
          width: "80%",
          outline: "none",
          padding: "0",
        }}
      />
      <IconButton
        type="submit"
        disabled={loading || inputValue.trim().length == 0}
      >
        <AddIcon
          fontSize="small"
          sx={{ color: theme.palette.text.secondary }}
        />
      </IconButton>
    </form>
  );
}

export default FormComponent;
