import { Box } from "@mui/material";
import threeDotsLoader from "../../../public/threeDotsLoader.svg";

function Loader({ status }) {
  return status ? (
    <Box
      sx={{
        backgroundColor: "transparent",
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        zIndex: "10",
      }}
    >
      <img src={threeDotsLoader} alt="Loader SVG" />
    </Box>
  ) : (
    <></>
  );
}

export default Loader;
