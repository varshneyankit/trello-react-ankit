import { useState } from "react";
import { Box, IconButton, Modal } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import FormModal from "./FormModal";
import theme from "../../styles/theme";

const style = {
  backgroundColor: theme.palette.background.main,
  border: `1px solid ${theme.palette.text.secondary}`,
  borderRadius: "8px",
  boxShadow: 24,
  display: "flex",
  flexDirection: "column",
  maxHeight: "80vh",
  width: 500,
  padding: "12px",
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
};

export default function BasicModal({
  containerText,
  containerStyle,
  header,
  inputPlaceholder,
  inputValue,
  handleInputChange,
  handleSubmit,
}) {
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <>
      <Box sx={containerStyle} onClick={handleOpen}>
        {containerText}
      </Box>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <IconButton
            sx={{
              color: theme.palette.text.secondary,
              alignSelf: "flex-end",
            }}
            onClick={() => handleClose()}
          >
            <CloseIcon />
          </IconButton>
          <FormModal
            header={header}
            inputPlaceholder={inputPlaceholder}
            inputValue={inputValue}
            handleInputChange={handleInputChange}
            handleSubmit={() => {
              handleSubmit();
              handleClose();
            }}
          />
        </Box>
      </Modal>
    </>
  );
}
