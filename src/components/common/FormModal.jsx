import { Button, Grid, TextField, Typography } from "@mui/material";
import theme from "../../styles/theme";

function FormModal({
  header,
  inputPlaceholder,
  inputValue,
  handleInputChange,
  handleSubmit,
}) {
  return (
    <Grid container display={"flex"}>
      <form
        style={{
          marginTop: "-20px",
          width: "100%",
        }}
        onSubmit={(event) => {
          event.preventDefault();
          handleSubmit();
        }}
      >
        <Grid
          item
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            marginBottom: 4,
            paddingLeft: 3,
            paddingRight: 3,
          }}
        >
          <Typography variant="h6" color={theme.palette.text.secondary}>
            {header}
          </Typography>
        </Grid>
        <Grid
          item
          sx={{
            display: "flex",
            flexDirection: "column",
            padding: "0px 24px",
          }}
        >
          <TextField
            id="outlined-basic"
            label={inputPlaceholder}
            variant="outlined"
            value={inputValue}
            onChange={(e) => handleInputChange(e.target.value)}
            InputLabelProps={{
              style: { color: theme.palette.text.secondary },
            }}
            autoFocus={true}
            sx={{
              "& 	.Mui-focused": {
                color: theme.palette.text.primary,
              },
              "& .MuiOutlinedInput-root": {
                "&.Mui-focused fieldset": {
                  borderColor: theme.palette.text.secondary,
                },
                "&:hover .MuiOutlinedInput-notchedOutline": {},
              },
              mb: 2,
            }}
          />
          <Button
            type="submit"
            variant="contained"
            disabled={inputValue.trim().length == 0}
            sx={{
              mb: 2,
              "&:disabled": {
                color: theme.palette.text.secondary,
              },
              "&:hover": {
                backgroundColor: theme.palette.text.primary,
              },
            }}
          >
            Create
          </Button>
        </Grid>
      </form>
    </Grid>
  );
}

export default FormModal;
