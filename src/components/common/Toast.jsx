import { Toaster } from "react-hot-toast";
import theme from "../../styles/theme";

function Toast() {
  return (
    <Toaster
      toastOptions={{
        error: {
          position: "top-right",
          iconTheme: {
            primary: theme.palette.text.delete,
            secondary: theme.palette.background.main,
          },
          style: {
            backgroundColor: theme.palette.background.default,
            color: theme.palette.text.primary,
            fontWeight: 500,
          },
        },
      }}
    />
  );
}

export default Toast;
