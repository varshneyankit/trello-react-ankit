import { useEffect, useState } from "react";
import toast from "react-hot-toast";
import {
  Box,
  Container,
  IconButton,
  Grid,
  ListItem,
  Typography,
} from "@mui/material";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import CloseIcon from "@mui/icons-material/Close";
import CreditCardIcon from "@mui/icons-material/CreditCard";
import FormComponent from "./common/FormComponent";
import Loader from "./common/Loader";
import CheckItemsSection from "./CheckItemsSection";
import { KEY, TOKEN } from "../constants";
import {
  handleDeleteRequest,
  handleGetRequest,
  handlePostRequest,
} from "../utils/helper";
import theme from "../styles/theme";

function ChecklistsSection({ cardName, cardId }) {
  const [checklists, setChecklists] = useState([]);
  const [checklistName, setChecklistName] = useState("");
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    function getChecklists() {
      const API_GET_CHECKLISTS = `https://api.trello.com/1/cards/${cardId}/checklists?key=${KEY}&token=${TOKEN}`;

      handleGetRequest(API_GET_CHECKLISTS)
        .then((response) => {
          setChecklists(response.data);
        })
        .catch((error) => {
          toast.error("Unable to get checklists!");
        })
        .finally(() => {
          setLoading(false);
        });
    }

    getChecklists();
  }, []);

  function createChecklist() {
    const newChecklistName = checklistName.trim();
    const API_CREATE_CHECKLIST = `https://api.trello.com/1/checklists?idCard=${cardId}&name=${newChecklistName}&key=${KEY}&token=${TOKEN}`;

    setLoading(true);

    handlePostRequest(API_CREATE_CHECKLIST)
      .then((response) => {
        setChecklists([...checklists, response.data]);
      })
      .catch((error) => {
        toast.error("Unable to create checklist!");
      })
      .finally(() => {
        setChecklistName("");
        setLoading(false);
      });
  }

  function deleteChecklist(checklistId) {
    const API_DELETE_CHECKLIST = `https://api.trello.com/1/checklists/${checklistId}?key=${KEY}&token=${TOKEN}`;

    setLoading(true);

    handleDeleteRequest(API_DELETE_CHECKLIST)
      .then((response) => {
        setChecklists(
          checklists.filter((checklist) => checklist.id !== checklistId)
        );
      })
      .catch((error) => {
        toast.error("Unable to delete checklist!");
      })
      .finally(() => {
        setLoading(false);
      });
  }

  return (
    <Container sx={{ position: "relative" }}>
      <Loader status={loading} />
      <Box display={"flex"} alignItems={"center"}>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            flexGrow: 1,
            marginTop: "-30px",
          }}
        >
          <IconButton>
            <CreditCardIcon sx={{ color: theme.palette.primary.main }} />
          </IconButton>
          <Typography variant="h6">{cardName}</Typography>
        </Box>
      </Box>
      <Box>
        <Box mt={2}>
          <Box display={"flex"} justifyContent={"center"}>
            <FormComponent
              loading={loading}
              inputPlaceholder={"Enter Checklist Name"}
              inputValue={checklistName}
              handleInputChange={setChecklistName}
              handleSubmit={createChecklist}
            />
          </Box>
          <Grid
            sx={{
              maxHeight: 400,
              mt: 2,
              overflow: "auto",
            }}
          >
            {checklists.map((checklist) => {
              let checklistId = checklist.id;
              let checklistName = checklist.name;

              return (
                <Box
                  key={checklistId}
                  mb={4}
                  sx={{
                    border: `1px solid ${theme.palette.text.secondary}`,
                    borderRadius: "4px",
                    padding: "12px 16px",
                  }}
                >
                  <ListItem sx={{ padding: 0 }}>
                    <Box display={"flex"} alignItems={"center"} flexGrow={1}>
                      <IconButton>
                        <CheckBoxIcon
                          sx={{ color: theme.palette.primary.main }}
                        />
                      </IconButton>
                      <Typography>{checklistName}</Typography>
                    </Box>
                    <IconButton
                      disabled={loading}
                      onClick={() => deleteChecklist(checklistId)}
                    >
                      <CloseIcon
                        sx={{
                          color: theme.palette.text.secondary,
                          "&:hover": {
                            color: theme.palette.text.delete,
                          },
                        }}
                      />
                    </IconButton>
                  </ListItem>
                  <CheckItemsSection
                    cardId={cardId}
                    checklistId={checklistId}
                  />
                </Box>
              );
            })}
          </Grid>
        </Box>
      </Box>
    </Container>
  );
}

export default ChecklistsSection;
