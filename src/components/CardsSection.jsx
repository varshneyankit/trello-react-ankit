import { useEffect, useState } from "react";
import toast from "react-hot-toast";
import { Box, IconButton, List, ListItem } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import FormComponent from "./common/FormComponent";
import Loader from "./common/Loader";
import Toast from "./common/Toast";
import ChecklistsModal from "./ChecklistsModal";
import ChecklistsSection from "./ChecklistsSection";
import { KEY, TOKEN } from "../constants";
import {
  handleDeleteRequest,
  handleGetRequest,
  handlePostRequest,
} from "../utils/helper";
import theme from "../styles/theme";

function CardsSection({ listId }) {
  const [cards, setCards] = useState([]);
  const [cardName, setCardName] = useState("");
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    function getCards() {
      const API_GET_CARDS = `https://api.trello.com/1/lists/${listId}/cards?key=${KEY}&token=${TOKEN}`;

      handleGetRequest(API_GET_CARDS)
        .then((response) => {
          setCards(response.data);
        })
        .catch((error) => {
          toast.error("Unable to get cards!");
        })
        .finally(() => {
          setLoading(false);
        });
    }

    getCards();
  }, []);

  function createCard() {
    const newCardName = cardName.trim();
    const API_CREATE_CARD = `https://api.trello.com/1/cards?idList=${listId}&name=${newCardName}&key=${KEY}&token=${TOKEN}`;

    setLoading(true);

    handlePostRequest(API_CREATE_CARD)
      .then((response) => {
        setCards([...cards, response.data]);
      })
      .catch((error) => {
        toast.error("Unable to create card!");
      })
      .finally(() => {
        setCardName("");
        setLoading(false);
      });
  }

  function deleteCard(cardId) {
    const API_DELETE_CARD = `https://api.trello.com/1/cards/${cardId}?key=${KEY}&token=${TOKEN}`;

    setLoading(true);

    handleDeleteRequest(API_DELETE_CARD)
      .then((response) => {
        setCards(cards.filter((card) => card.id !== cardId));
      })
      .catch((error) => {
        toast.error("Unable to delete card!");
      })
      .finally(() => {
        setLoading(false);
      });
  }

  return (
    <Box position={"relative"}>
      <Loader status={loading} />
      <Toast />
      <Box>
        <FormComponent
          loading={loading}
          inputPlaceholder={"Enter Card Name"}
          inputValue={cardName}
          handleInputChange={setCardName}
          handleSubmit={createCard}
        />
        <List sx={{ mt: 1 }}>
          {cards.map((card) => {
            let cardId = card.id;
            let cardName = card.name;

            return (
              <ListItem
                key={cardId}
                sx={{
                  backgroundColor: theme.palette.background.default,
                  borderRadius: "4px",
                  fontWeight: 400,
                  padding: "4px 12px",
                  mt: 1,
                }}
              >
                <ChecklistsModal
                  containerText={cardName}
                  containerStyle={{
                    cursor: "pointer",
                    padding: "8px 0",
                    width: "100%",
                  }}
                >
                  <ChecklistsSection cardName={cardName} cardId={cardId} />
                </ChecklistsModal>
                <IconButton
                  disabled={loading}
                  onClick={() => deleteCard(cardId)}
                  aria-label="delete"
                >
                  <CloseIcon
                    fontSize="small"
                    sx={{
                      color: theme.palette.text.secondary,
                      "&:hover": {
                        color: theme.palette.text.delete,
                      },
                    }}
                  />
                </IconButton>
              </ListItem>
            );
          })}
        </List>
      </Box>
    </Box>
  );
}

export default CardsSection;
