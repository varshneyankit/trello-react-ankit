import { useState, useEffect } from "react";
import toast from "react-hot-toast";
import {
  Box,
  Checkbox,
  IconButton,
  List,
  ListItem,
  ListItemText,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import FormComponent from "./common/FormComponent";
import Loader from "./common/Loader";
import ProgressBar from "./ProgressBar";
import { KEY, TOKEN } from "../constants";
import {
  getCheckedItemsPercent,
  handleDeleteRequest,
  handleGetRequest,
  handlePostRequest,
  handlePutRequest,
} from "../utils/helper";
import theme from "../styles/theme";

function CheckItemsSection({ cardId, checklistId }) {
  const [checkItems, setCheckItems] = useState([]);
  const [checkItemName, setCheckItemName] = useState("");
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    function getCheckItems() {
      const API_GET_CHECKITEMS = `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${KEY}&token=${TOKEN}`;

      handleGetRequest(API_GET_CHECKITEMS)
        .then((response) => {
          setCheckItems(response.data);
        })
        .catch((error) => {
          toast.error("Unable to get checkitems!");
        })
        .finally(() => {
          setLoading(false);
        });
    }

    getCheckItems();
  }, []);

  function createCheckItem() {
    const newCheckItemName = checkItemName.trim();
    const API_CREATE_CHECKITEM = `https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${newCheckItemName}&key=${KEY}&token=${TOKEN}`;

    setLoading(true);

    handlePostRequest(API_CREATE_CHECKITEM)
      .then((response) => {
        setCheckItems([...checkItems, response.data]);
      })
      .catch((error) => {
        toast.error("Unable to create checkitem!");
      })
      .finally(() => {
        setCheckItemName("");
        setLoading(false);
      });
  }

  function deleteCheckItem(checkItemId) {
    const API_DELETE_CHECKITEM = `https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkItemId}?key=${KEY}&token=${TOKEN}`;

    setLoading(true);

    handleDeleteRequest(API_DELETE_CHECKITEM)
      .then((response) => {
        setCheckItems(
          checkItems.filter((checkItem) => checkItem.id !== checkItemId)
        );
      })
      .catch((error) => {
        toast.error("Unable to delete checkitem!");
      })
      .finally(() => {
        setLoading(false);
      });
  }

  function updateCheckItem(checkItemId, checkItemState) {
    const updatedState =
      checkItemState == "incomplete" ? "complete" : "incomplete";
    const API_UPDATE_CHECKITEM = `https://api.trello.com/1/cards/${cardId}/checklist/${checklistId}/checkItem/${checkItemId}?state=${updatedState}&key=${KEY}&token=${TOKEN}`;

    setLoading(true);

    handlePutRequest(API_UPDATE_CHECKITEM)
      .then((response) => {
        setCheckItems(
          checkItems.map((checkItem) => {
            if (checkItem.id === checkItemId) {
              return {
                ...checkItem,
                state: updatedState,
              };
            } else {
              return checkItem;
            }
          })
        );
      })
      .catch((error) => {
        toast.error("Unable to update checkitem!");
      })
      .finally(() => {
        setLoading(false);
      });
  }

  let percent = getCheckedItemsPercent(checkItems);

  return (
    <Box position={"relative"} sx={{ mt: 3 }}>
      <Loader status={loading} />
      <List>
        <FormComponent
          loading={loading}
          inputPlaceholder={"Enter Checkitem Name"}
          inputValue={checkItemName}
          handleInputChange={setCheckItemName}
          handleSubmit={createCheckItem}
        />
      </List>

      <Box>
        <List sx={{ margin: "8px 0" }}>
          <ProgressBar percent={Number.isNaN(percent) ? 0 : percent} />
          {checkItems.map((checkItem) => {
            let checkItemId = checkItem.id;
            let checkItemName = checkItem.name;
            let checkItemState = checkItem.state;

            return (
              <ListItem
                key={checkItemId}
                sx={{
                  backgroundColor: theme.palette.background.default,
                  borderRadius: "4px",
                  padding: "0",
                  mt: 2,
                }}
              >
                <Checkbox
                  onClick={() => updateCheckItem(checkItemId, checkItemState)}
                  checked={checkItemState == "complete"}
                />
                <ListItemText>{checkItemName}</ListItemText>
                <IconButton
                  disabled={loading}
                  onClick={() => deleteCheckItem(checkItemId)}
                >
                  <CloseIcon
                    sx={{
                      color: theme.palette.text.secondary,
                      "&:hover": {
                        color: theme.palette.text.delete,
                      },
                    }}
                  />
                </IconButton>
              </ListItem>
            );
          })}
        </List>
      </Box>
    </Box>
  );
}

export default CheckItemsSection;
